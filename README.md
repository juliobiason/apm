# APM

A Stupid Simple APM for Python

## What does it do?

`apm` can be used to keep track how many times a function is called and how
long it takes to complete.

To track a function, you need to decorate it with

	import apm

	@apm.monitor
	def my_function():
		# do some stuff

To check the stats about the function, you can use `apm.execution`; it will
return a list of the called methods, with their `call_count` about the number
of calls, `avg_time` for the average time it took to execute the function,
`low_time` with the lowest time it took to complete and `high_time` with the
highest time.

## TODO

* This still needs a proper `setup.py`.
* There should be a way to see the execution tree in a better way; currently,
  if `func1` was called by `func2` -- and both are being monitored --, you'll
  get a call for `func2|func1`. This is sub-optimal, although you can get your
  results.
