"""Main function."""

from __future__ import print_function

import time
import pprint
import random

import apm


@apm.monitor
def decision1():
    print ("I'm one")


@apm.monitor
def decision2():
    print ("I'm two")


@apm.monitor
def decision():
    if random.random() < 0.5:
        decision1()
    else:
        decision2()


@apm.monitor
def subfunction():
    print("I do nothing")


@apm.monitor
def testing():
    time.sleep(1)
    subfunction()
    return 43


if __name__ == "__main__":
    print("Result: " + str(testing()))
    testing()       # ignore results
    testing()       # again
    subfunction()   # just because

    for _ in xrange(50):
        decision()

    pprint.pprint(apm.execution())
